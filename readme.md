# YouTube Thumbnail Fetcher

Fetches a thumbnail of current YouTube page.

## Getting Started

git clone https://davidjbarnes@bitbucket.org/davidjbarnes/youtube-thumbnail-fetcher-chrome-extension.git

### Installing

Open the Extension Management page by navigating to chrome://extensions. Enable Developer Mode by clicking the toggle switch next to Developer mode. Click the LOAD UNPACKED button and select the extension directory.

## Contributing

Email me at: nullableint@gmail.com

## Author

* **David J Barnes**

## License

This project is licensed under the MIT License

## Acknowledgments

* https://developer.chrome.com/extensions/getstarted
* https://developers.google.com/youtube/v3/