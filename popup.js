window.addEventListener("DOMContentLoaded", function() {
    document.getElementById("btn").addEventListener("click", _ => {
    
        chrome.tabs.query({
            active: true,
            currentWindow: true
        }, ([currentTab]) => {
            const tabId = currentTab.id;

            chrome.tabs.sendMessage(tabId, { method: "getImageAsUrl" }, (response) => {
                const imageAsUrl = response.imageAsUrl;

                document.getElementById("img").src = imageAsUrl;
            });
        });
  });
});