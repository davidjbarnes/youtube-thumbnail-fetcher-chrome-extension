chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    if (request.method && (request.method === "getImageAsUrl")) {
        const imageAsUrl = getImageAsUrl();

        sendResponse({ imageAsUrl: imageAsUrl });
    }
});

function getImageAsUrl() {
    const videoId = document.location.search.split("=")[1];
    return "https://img.youtube.com/vi/"+videoId+"/0.jpg";
}